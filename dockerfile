FROM alpine

COPY /bin/api /app/api

ENV DB_USER DB_PASSWORD DB_HOST DB_DATABASE

CMD ["/app/api"]