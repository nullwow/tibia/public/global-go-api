package server

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.com/nullwow/tibia/global-go-api/routes"
)

type Server struct {
	port  string
	route *gin.Engine
}

func (this *Server) New() {
	routes := new(routes.Routes)
	router := routes.New()

	this.port = "8080"
	// Instantiate middlewares
	r := gin.New()
	config := cors.DefaultConfig()
	config.AllowHeaders = []string{"*"}
	config.AllowOrigins = []string{"*"}
	r.Use(cors.New(config))

	this.route = router.ConfigRoutes(r)

	log.Info("Server rodando na porta: " + this.port)
	log.Fatal(this.route.Run(":" + this.port))
}
