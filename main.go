package main

import (
	"gitlab.com/nullwow/tibia/global-go-api/server"
)

func main() {
	server := new(server.Server)
	server.New()
}
