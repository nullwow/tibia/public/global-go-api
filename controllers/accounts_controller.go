package controllers

import (
	"crypto/sha1"
	"encoding/hex"
	"time"

	"gitlab.com/nullwow/tibia/global-go-api/database"
	"gitlab.com/nullwow/tibia/global-go-api/models"
	"gorm.io/gorm"
)

type Controller interface {
	List()
	Create()
	Get()
	Update()
}
type AccountsController struct {
	db *gorm.DB
}

func encrtyptPasswords(password string) string {
	// Update password with hashed one
	h := sha1.New()
	hashedPassword := []byte(password)
	h.Write(hashedPassword)
	return hex.EncodeToString(h.Sum(nil))
}

func (this *AccountsController) New() *AccountsController {
	var manager = new(database.DatabaseManager)
	this.db = manager.GetInstance().OtsDatabase.Get()
	return this
}

func (a *AccountsController) List() (accounts []models.Accounts, err error) {
	err = a.db.Find(&accounts).Error
	return accounts, err
}

func (a *AccountsController) Get(id int) (accounts models.Accounts, err error) {
	err = a.db.First(&accounts, id).Error
	return accounts, err
}

func (a *AccountsController) GetByName(name string) (account models.Accounts, err error) {
	err = a.db.Where("name = ?", name).First(&account).Error
	return account, err
}

func (a *AccountsController) Login(acc models.Login) (account *models.Accounts, err error) {
	h := sha1.New()
	hashedPassword := []byte(acc.Password)
	h.Write(hashedPassword)
	tmp_pwd := hex.EncodeToString(h.Sum(nil))

	err = a.db.Where("name = ?", acc.Name).First(&account).Error

	if account.Password == tmp_pwd {
		return account, err
	}
	return &models.Accounts{}, err
}

func (a *AccountsController) Create(account models.Accounts) (err error) {

	account.Password = encrtyptPasswords(account.Password)
	account.Creation = time.Now().Unix()

	return a.db.Create(&account).Error
}
