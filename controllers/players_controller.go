package controllers

import (
	"errors"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/nullwow/tibia/global-go-api/database"
	"gitlab.com/nullwow/tibia/global-go-api/locale"
	"gitlab.com/nullwow/tibia/global-go-api/models"
	"gorm.io/gorm"
)

type PlayersController struct {
	db *gorm.DB
}

func (this *PlayersController) New() *PlayersController {
	var manager = new(database.DatabaseManager)
	this.db = manager.GetInstance().OtsDatabase.Get()
	return this
}

func (this *PlayersController) Get(id int) (player models.Players, err error) {
	err = this.db.First(&player, id).Error
	return player, err
}

func (this *PlayersController) Create(p *models.Players) (player *models.Players, err error) {
	exists := this.Exists(p.Name)
	if exists {
		return (&models.Players{}), errors.New(locale.English[locale.LOCALE_PLAYER_ALREADY_EXISTS])
	}
	// Set defaults
	p.Conditions = []byte{}
	p.CreateDate = time.Now().Unix()

	result := this.db.Create(&p)
	if result.RowsAffected != 1 || result.Error != nil {
		return (&models.Players{}), result.Error
	}
	return p, nil
}

func (this *PlayersController) GetByName(name string) (player *models.Players, err error) {
	err = this.db.Where("name = ?", name).First(&player).Error
	return player, err
}

func (this *PlayersController) List() (players *[]models.Players, err error) {
	err = this.db.Find(&players).Limit(20).Error
	return players, err
}

func (this *PlayersController) Exists(name string) bool {
	count := int64(0)
	err := this.db.Model(&models.Players{}).Where("name = ?", name).Count(&count).Error
	if err != nil {
		log.Fatal("Error while chekcing for player's name. Check it.")
		return true
	}
	return count > 0
}

func (this *PlayersController) GetPlayersByAccount(acc *models.Accounts) (players *[]models.Players, err error) {
	err = this.db.Model(&models.Players{}).Where("account_id = ?", acc.ID).Find(&players).Error
	return players, err
}
