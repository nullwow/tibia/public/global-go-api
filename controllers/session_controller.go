package controllers

import (
	"context"
	"encoding/json"

	"github.com/google/uuid"
	"github.com/redis/go-redis/v9"
	log "github.com/sirupsen/logrus"
	"gitlab.com/nullwow/tibia/global-go-api/models"
	local_redis "gitlab.com/nullwow/tibia/global-go-api/redis"
)

const (
	REDIS_TTL = 300 * 1000
	BASE_TTL  = 1000000 * REDIS_TTL
)

type SessionController struct {
	redis *redis.Client
}

func (this *SessionController) New() *SessionController {
	var redis_manager = new(local_redis.RedisManager)
	this.redis = redis_manager.GetInstance().Client.Get()
	return this
}

func (this *SessionController) Create(account *models.Accounts) (s *models.Session, err error) {
	s = &models.Session{
		Hash:    uuid.New().String(),
		Account: account,
	}
	ctx := context.Background()
	str, err := json.Marshal(s)
	if err != nil {
		panic("Deu bosta")
	}
	status := this.redis.Set(ctx, s.Hash, str, BASE_TTL)
	if status.Err() != nil {
		return &models.Session{}, status.Err()
	}
	return s, err
}

func (this *SessionController) Get(hash string) (s *models.Session, err error) {
	ctx := context.Background()
	val, err := this.redis.Get(ctx, hash).Result()
	if val != "" {
		err := json.Unmarshal([]byte(val), &s)
		if err != nil {
			return &models.Session{}, err
		}
	}
	if err == nil && s.Account != (&models.Accounts{}) {
		this.UpdateTTL(hash)
		return s, err
	}
	return &models.Session{}, err
}

func (this *SessionController) Delete(hash string) (err error) {
	ctx := context.Background()
	n, err := this.redis.Del(ctx, hash).Result()
	if n > 1 {
		log.Warning("More then one key were deleted. Check this. Key: " + hash)
	} else if n == 0 {
		log.Info("No session found when deleting.")
	}
	return err
}

func (this *SessionController) Exists(hash string) bool {
	ctx := context.Background()
	exists, err := this.redis.Exists(ctx, hash).Result()
	if err != nil {
		log.Fatal("Error while getting from redis, take a look. hash: " + hash)
		return false
	}
	return exists > 0
}

func (this *SessionController) UpdateTTL(hash string) {
	ctx := context.Background()
	result, err := this.redis.Expire(ctx, hash, BASE_TTL).Result()
	if err != nil {
		log.Warning("Error while updating ttl. Err: " + err.Error())
	}
	if !result {
		log.Warning("Fail on set expire to hash: " + hash)
	}
}
