package locale

type LocaleType int64

const (
	LOCALE_PLAYER_NOT_FOUND           LocaleType = 0
	LOCALE_PLAYER_ALREADY_EXISTS                 = 1
	LOCALE_WRONG_USERNAME_OR_PASSWORD            = 2
	LOCALE_LOGIN_FIRST                           = 3
	LOCALE_SERVER_ERROR                          = 4
	LOCALE_PLAYER_CREATED                        = 5
	LOCALE_PLAYER_INFORMATION_WRONG              = 6
	LOCALE_PLAYER_NAME_TOO_SMALL                 = 7
)

var English = map[LocaleType]string{
	LOCALE_PLAYER_NOT_FOUND:           "Player not Found.",
	LOCALE_PLAYER_ALREADY_EXISTS:      "Player already exists.",
	LOCALE_WRONG_USERNAME_OR_PASSWORD: "Wrong Username or password.",
	LOCALE_LOGIN_FIRST:                "You need to be logged in to do this action.",
	LOCALE_SERVER_ERROR:               "Server Error, contact administrator.",
	LOCALE_PLAYER_CREATED:             "Player created successfully.",
	LOCALE_PLAYER_INFORMATION_WRONG:   "Player information wrong, try again.",
	LOCALE_PLAYER_NAME_TOO_SMALL:      "Player name too small",
}
