package models

type Session struct {
	Hash    string    `json:"hash"`
	Account *Accounts `json:"account"`
}
