package models

type Players struct {
	ID         int    `json:"id" gorm:"primary_key, AUTO_INCREMENT"`
	Account_id uint16 `json:"account_id" gorm:"type:uuid;column:account_id;not null;"`
	Group_ID   uint   `json:"group_id" gorm:"default:1"`

	Level      uint   `json:"level" default:"1" gorm:"default:1"`
	Experience uint64 `json:"experience"`

	Name     string `json:"name" gorm:"index"`
	Sex      uint   `json:"sex,string"`
	Vocation uint   `json:"vocation" default:"1" gorm:"default:0"`

	Town_ID   uint `json:"town_id"`
	Lastlogin uint `json:"lastlogin"`
	Lastip    uint `json:"lastip"`
	Deletion  bool `json:"deletion"`

	Posx int
	Posy int
	Posz int

	CreateDate int64 `gorm:"column:create_date"`

	Hidden bool `json:"hidden" gorm:"column:hide_char"`

	Conditions []byte `json:"-" gorm:"default:0"`
}
