package models

type Accounts struct {
	ID       uint16 `json:"id" gorm:"primary_key, AUTO_INCREMENT"`
	Name     string `json:"name"`
	Password string `json:"-"`
	Coins    uint   `json:"coins" default:"0"`
	Type     int    `json:"type" gorm="default:1"`
	Email    string `json:"email"`
	Creation int64  `json:"creation"`
	Blocked  bool   `json:"blocked" gorm="default false"`
}

type Login struct {
	Name     string `json:"name"`
	Password string `json:"password"`
}
