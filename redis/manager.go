package local_redis

import (
	"os"
)

type RedisManager struct {
	RedisInfo *RedisConnectionInfo
	Client    *Redis
	Instance  *RedisManager
}

func (this *RedisManager) new() {
	this.RedisInfo = &RedisConnectionInfo{
		os.Getenv("REDIS_HOST"),
		os.Getenv("REDIS_PORT"),
		os.Getenv("REDIS_USER"),
		os.Getenv("REDIS_PASSWORD"),
	}
	var r = new(Redis)
	this.Client = r.New(this.RedisInfo)
	this.Instance = this
}

func (this *RedisManager) GetInstance() *RedisManager {
	if this.Instance == nil {
		this.new()
	}
	return this.Instance
}
