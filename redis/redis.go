package local_redis

import "github.com/redis/go-redis/v9"

type Redis struct {
	client *redis.Client
}

type RedisConnectionInfo struct {
	Host     string
	Port     string `Default:"6379"`
	Username string
	Password string
}

func (this *Redis) New(info *RedisConnectionInfo) *Redis {
	var uri = "redis://" + info.Host + ":" + info.Port + "/0"
	if info.Username != "" {
		uri = "redis://" + info.Username + ":" + info.Password + "@" + info.Host + ":" + info.Port + "/0"
	}
	opt, err := redis.ParseURL(uri)
	if err != nil {
		panic(err)
	}

	this.client = redis.NewClient(opt)
	return this
}

func (this *Redis) Get() *redis.Client {
	return this.client
}
