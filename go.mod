module gitlab.com/nullwow/tibia/global-go-api

go 1.13

require (
	github.com/gin-contrib/cors v1.4.0 // indirect
	github.com/gin-gonic/gin v1.9.0
	github.com/google/uuid v1.3.0 // indirect
	github.com/redis/go-redis/v9 v9.0.3 // indirect
	github.com/sirupsen/logrus v1.9.0 // indirect
	github.com/thanhhh/gin-requestid v0.0.0-20180527051759-221db8554b0d
	gorm.io/driver/mysql v1.2.3
	gorm.io/gorm v1.22.5
)
