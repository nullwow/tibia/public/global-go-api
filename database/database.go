package database

import (
	"fmt"
	"log"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type Database struct {
	connection *gorm.DB
}

type DbConnectionInfo struct {
	User     string
	Password string
	Host     string
	Database string
}

func (this *Database) New(info *DbConnectionInfo) *Database {
	dbURI := fmt.Sprintf(`%s:%s@tcp(%s)/%s?charset=utf8&parseTime=True&loc=Local`, info.User, info.Password, info.Host, info.Database)
	db, err := gorm.Open(mysql.Open(dbURI), &gorm.Config{})
	if err != nil {
		log.Fatal("Failed to init Database", err)
	}

	config, _ := db.DB()
	config.SetConnMaxLifetime(10)
	config.SetMaxIdleConns(10)
	config.SetMaxOpenConns(30)
	config.SetConnMaxLifetime(time.Hour)

	this.connection = db
	return this
}

func (this *Database) Get() *gorm.DB {
	return this.connection
}
