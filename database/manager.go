package database

import "os"

type DatabaseManager struct {
	OtsInfo     *DbConnectionInfo
	OtsDatabase *Database
	Instance    *DatabaseManager
}

func (this *DatabaseManager) new() {
	this.OtsInfo = &DbConnectionInfo{
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_DATABASE"),
	}
	var otsDB = new(Database)
	this.OtsDatabase = otsDB.New(this.OtsInfo)
	this.Instance = this
}

func (this *DatabaseManager) GetInstance() *DatabaseManager {
	if this.Instance == nil {
		this.new()
	}
	return this.Instance
}
