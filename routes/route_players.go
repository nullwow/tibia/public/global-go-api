package routes

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.com/nullwow/tibia/global-go-api/controllers"
	"gitlab.com/nullwow/tibia/global-go-api/locale"
	"gitlab.com/nullwow/tibia/global-go-api/models"
)

const (
	MINIMUM_PLAYER_NAME_SIZE int = 5
)

type PlayersRoute struct {
	controller   *controllers.PlayersController
	s_controller *controllers.SessionController
}

func (this *PlayersRoute) New() *PlayersRoute {
	controller := new(controllers.PlayersController)
	s_controller := new(controllers.SessionController)
	this.controller = controller.New()
	this.s_controller = s_controller.New()
	return this
}

func (p *PlayersRoute) Get(c *gin.Context) {
	id := c.Param("id")
	newid, err := strconv.Atoi(id)
	if err != nil {
		ReturnGinError(c, http.StatusBadRequest, "ID must be integer!")
		return
	}

	players, err := p.controller.Get(newid)
	if err != nil {
		ReturnGinError(c, http.StatusBadRequest, "Player not found")
		log.Print("Error: " + err.Error())
		return
	}
	c.JSON(http.StatusOK, players)
}

func (p *PlayersRoute) GetByName(c *gin.Context) {
	name := c.Param("name")
	players, err := p.controller.GetByName(name)
	if err != nil {
		ReturnGinError(c, http.StatusBadRequest, "Player not found")
		log.Print("Error: " + err.Error())
		return
	}
	c.JSON(http.StatusOK, players)
}

func (p *PlayersRoute) List(c *gin.Context) {
	players, err := p.controller.List()
	if err != nil {
		ReturnGinError(c, http.StatusInternalServerError, "Erro inesperado. Contate um administrador.")
		log.Print("Error: " + c.Request.Header["Request-ID"][0] + err.Error())
		return
	}
	c.JSON(http.StatusOK, players)
}

func (this *PlayersRoute) Create(c *gin.Context) {
	//var players *models.Players
	var player *models.Players

	if err := c.BindJSON(&player); err != nil {
		ReturnGinError(c, http.StatusNotAcceptable, locale.English[locale.LOCALE_PLAYER_INFORMATION_WRONG])
		return
	}

	if len(player.Name) < MINIMUM_PLAYER_NAME_SIZE {
		ReturnGinError(c, http.StatusBadRequest, locale.English[locale.LOCALE_PLAYER_NAME_TOO_SMALL])
		return
	}

	// Defaults
	player.Level = 1
	player.Town_ID = 0
	player.Vocation = 0

	// Get account number from session
	session := c.MustGet(SESSION_HEADER).(*models.Session)
	player.Account_id = session.Account.ID

	// Create
	player, err := this.controller.Create(player)
	if err != nil {
		switch err.Error() {
		case locale.English[locale.LOCALE_PLAYER_ALREADY_EXISTS]:
			ReturnGinError(c, http.StatusConflict, err.Error())
		default:
			ReturnGinError(c, http.StatusInternalServerError, locale.English[locale.LOCALE_SERVER_ERROR])
			log.Warning("Error: " + err.Error())
		}
		return
	}

	c.JSON(http.StatusOK, gin.H{"player": player, "message": locale.English[locale.LOCALE_PLAYER_CREATED]})
}
