package routes

import (
	"net/http"
	"net/mail"
	"strconv"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.com/nullwow/tibia/global-go-api/controllers"
	"gitlab.com/nullwow/tibia/global-go-api/models"
)

type AccountsRoute struct {
	controller   *controllers.AccountsController
	p_controller *controllers.PlayersController
}

func (this *AccountsRoute) New() *AccountsRoute {
	accountsController := new(controllers.AccountsController)
	p_controller := new(controllers.PlayersController)
	this.p_controller = p_controller.New()
	this.controller = accountsController.New()
	return this
}

func (this *AccountsRoute) List(c *gin.Context) {
	accounts, err := this.controller.List()
	if err != nil {
		ReturnGinError(c, http.StatusInternalServerError, "Error getting accounts list. Contact an administrator")
		log.Print("Error: " + c.Request.Header["Request-ID"][0] + err.Error())
		return
	}
	c.JSON(http.StatusOK, accounts)
}

func (this *AccountsRoute) Get(c *gin.Context) {
	id := c.Param("id")
	newid, err := strconv.Atoi(id)
	if err != nil {
		ReturnGinError(c, http.StatusBadRequest, "ID must be integer")
		return
	}
	accounts, err := this.controller.Get(newid)
	if err != nil {
		ReturnGinError(c, http.StatusNotFound, "Account not found.")
		return
	}
	c.JSON(http.StatusOK, accounts)
}

func (this *AccountsRoute) GetByName(c *gin.Context) {
	name := c.Param("name")
	account, err := this.controller.GetByName(name)
	if err != nil {
		ReturnGinError(c, http.StatusBadRequest, "Account name not found.")
		return
	}
	c.JSON(http.StatusOK, account)
}

func (a *AccountsRoute) Create(c *gin.Context) {
	var account models.Accounts

	if err := c.BindJSON(&account); err != nil {
		ReturnGinError(c, http.StatusBadRequest, "Error parsing inputs. Please review it!")
		return
	}
	_, err := mail.ParseAddress(account.Email)

	if account.Email == "" || account.Password == "" || account.Name == "" || err != nil {
		ReturnGinError(c, http.StatusNotAcceptable, "Invalid attributes. Please review it.")
		return
	}

	// Create Account
	if err := a.controller.Create(account); err != nil {
		ReturnGinError(c, http.StatusInternalServerError, "Erro creating account.")
		log.Print("Error: " + c.Request.Header["Request-ID"][0] + err.Error())
		return
	}
	c.JSON(http.StatusOK, "Account create successfuly!")
}

func (this *AccountsRoute) GetPlayers(c *gin.Context) {
	session := c.MustGet(SESSION_HEADER).(*models.Session)

	players, err := this.p_controller.GetPlayersByAccount(session.Account)
	if err != nil {
		return
	}
	c.JSON(http.StatusOK, players)
}
