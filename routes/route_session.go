package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/nullwow/tibia/global-go-api/controllers"
	"gitlab.com/nullwow/tibia/global-go-api/locale"
	"gitlab.com/nullwow/tibia/global-go-api/models"
)

const (
	SESSION_HEADER = "NULLSERVERS_SESSION"
)

type SessionRoute struct {
	controller         *controllers.SessionController
	account_controller *controllers.AccountsController
}

func (this *SessionRoute) New() *SessionRoute {
	controller := new(controllers.SessionController)
	account_controller := new(controllers.AccountsController)
	this.account_controller = account_controller.New()
	this.controller = controller.New()
	return this
}

func (this *SessionRoute) Get(c *gin.Context) {
	hash := c.GetHeader(SESSION_HEADER)
	s, err := this.controller.Get(hash)
	if err != nil {
		ReturnGinError(c, http.StatusNotFound, "Session not found.")
		return
	}
	c.JSON(http.StatusOK, s)
}

func (this *SessionRoute) Login(c *gin.Context) {
	j_account := new(models.Login)
	if err := c.BindJSON(&j_account); err != nil {
		c.JSON(http.StatusBadRequest, "Erro ao identificar os campos")
		return
	}

	account, err := this.account_controller.Login(*j_account)
	if err != nil {
		ReturnGinError(c, http.StatusUnauthorized, locale.English[locale.LOCALE_WRONG_USERNAME_OR_PASSWORD])
		return
	}
	if account != (&models.Accounts{}) {
		session, err := this.controller.Create(account)
		if err == nil && session != (&models.Session{}) {
			c.JSON(http.StatusOK, session)
		} else {
			c.JSON(http.StatusInternalServerError, err)
		}
		return
	}
	c.JSON(http.StatusUnauthorized, locale.English[locale.LOCALE_WRONG_USERNAME_OR_PASSWORD])
}

func (this *SessionRoute) Logout(c *gin.Context) {
	hash := c.GetHeader(SESSION_HEADER)
	err := this.controller.Delete(hash)
	if err != nil {
		ReturnGinError(c, http.StatusInternalServerError, err.Error())
		return
	}
	c.Status(http.StatusNoContent)
}
