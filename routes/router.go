package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	requestid "github.com/thanhhh/gin-requestid"
	"gitlab.com/nullwow/tibia/global-go-api/locale"
)

type Routes struct {
	accounts *AccountsRoute
	players  *PlayersRoute
	session  *SessionRoute
}

func ReturnGinError(c *gin.Context, status int, message string) {
	c.JSON(status, gin.H{
		"error":      message,
		"request-id": requestid.GetReqID(c),
	})
}

func (this *Routes) New() *Routes {
	// Instatiate
	accountsRoute := new(AccountsRoute)
	playersRoute := new(PlayersRoute)
	sessionRoute := new(SessionRoute)

	this.players = playersRoute.New()
	this.accounts = accountsRoute.New()
	this.session = sessionRoute.New()
	return this
}

func (this *Routes) ValidateSession(c *gin.Context) {
	hash := c.GetHeader(SESSION_HEADER)
	session, err := this.session.controller.Get(hash)
	if hash == "" || err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, locale.English[locale.LOCALE_LOGIN_FIRST])
		return
	}
	c.Set(SESSION_HEADER, session)
	c.Next()
}

func (r *Routes) ConfigRoutes(router *gin.Engine) *gin.Engine {

	main := router.Group("api/v1")
	{
		players := main.Group("players")
		{
			players.GET("/:id", r.players.Get)
			players.GET("/", r.players.List)
			players.POST("/", r.ValidateSession, r.players.Create)
			p_name := players.Group("name")
			{
				p_name.GET("/:name", r.players.GetByName)
			}
		}
		account := main.Group("account")
		{
			account.GET("/", r.accounts.List)
			account.GET("/:id", r.accounts.Get)
			account.POST("/", r.accounts.Create)
			a_name := account.Group("name")
			{
				a_name.GET("/:name", r.accounts.GetByName)
			}
			a_players := account.Group("players")
			{
				a_players.GET("/", r.ValidateSession, r.accounts.GetPlayers)
			}
		}
		sessions := main.Group("session")
		{
			sessions.GET("/", r.session.Get)
			sessions.POST("/login", r.session.Login)
			sessions.DELETE("/", r.session.Logout)
		}
	}
	return router
}
